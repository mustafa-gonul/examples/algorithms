#include <gtest/gtest.h>
#include <nimu/algorithms/graph.hpp>
#include <nimu/algorithms/dfs_simple.hpp>

using namespace nimu::algorithms;

TEST(nimu_algorithms_dfs_simple, graph_2)
{
  graph g{2};

  dfs_simple r1{g, 0};
  EXPECT_FALSE(r1.connected());

  dfs_simple r2{g, 1};
  EXPECT_FALSE(r2.connected());

  g.add(0, 1);

  dfs_simple r3{g, 0};
  EXPECT_TRUE(r3.connected());

  dfs_simple r4{g, 1};
  EXPECT_TRUE(r4.connected());
}
