#pragma once

#include <nimu/algorithms/common.hpp>

#include <vector>


namespace nimu {
namespace algorithms {


class dfs_path {
private:
  using path_type = std::vector<index_type>;

public:
  dfs_path(graph const&, index_type);

  bool visited(index_type) const;
  bool connected() const;

  size_type size() const;
  path_type path(index_type) const;

private:
  void dfs(graph const&, index_type);

private:
  std::vector<bool> visited_;
  std::vector<index_type> edges_;
  size_type size_;
};


}
}