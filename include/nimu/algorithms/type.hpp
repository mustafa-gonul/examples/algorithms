#pragma once

namespace nimu {
namespace algorithms {

using index_type = unsigned;
using size_type  = unsigned;

constexpr index_type invalid_index = -1;

}
}
