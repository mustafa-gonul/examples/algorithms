#pragma once

#include <nimu/algorithms/common.hpp>

#include <vector>


namespace nimu {
namespace algorithms {


class dfs_simple {
public:
  dfs_simple(graph const&, index_type);

  bool visited(index_type) const;
  bool connected() const;

  size_type size() const;

private:
  void dfs(graph const&, index_type);

private:
  std::vector<bool> visited_;
  size_type size_;
};


}
}