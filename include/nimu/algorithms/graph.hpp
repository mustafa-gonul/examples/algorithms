#pragma once

#include <nimu/algorithms/common.hpp>

#include <vector>
#include <list>
#include <iostream>


namespace nimu {
namespace algorithms {


class graph {
private:
  using list = std::list<index_type>;
  using adjacency_lists = std::vector<list>;

public:
  graph(size_type vertex_size);

  size_type vertex_size() const { return v_size; }
  size_type edge_size() const   { return e_size; }

  void add(index_type, index_type);

  auto cbegin(index_type i) const -> typename list::const_iterator { return al[i].cbegin(); }
  auto cend(index_type i) const -> typename list::const_iterator   { return al[i].cend();   }

private:
  adjacency_lists al;
  size_type v_size;
  size_type e_size;

  friend std::ostream& operator<<(std::ostream& os, graph const& g);
};


}
}
