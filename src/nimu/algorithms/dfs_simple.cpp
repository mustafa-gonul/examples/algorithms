#include <nimu/algorithms/dfs_simple.hpp>
#include <nimu/algorithms/graph.hpp>

namespace nimu {
namespace algorithms {


dfs_simple::dfs_simple(graph const& g, index_type i)
  : visited_(g.vertex_size())
  , size_{0}
{
  dfs(g, i);
}

bool dfs_simple::visited(index_type i) const
{
  return visited_[i];
}

bool dfs_simple::connected() const
{
  return size_ == visited_.size();
}

size_type dfs_simple::size() const
{
  return size_;
}

void dfs_simple::dfs(graph const& g, index_type current)
{
  visited_[current] = true;
  ++size_;

  for (auto i = g.cbegin(current); i != g.cend(current); ++i) {
    auto next = *i;
    if (!visited(next))
      dfs(g, next);
  }

}


}
}
