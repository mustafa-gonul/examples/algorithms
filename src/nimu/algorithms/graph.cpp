#include <nimu/algorithms/graph.hpp>
#include <iostream>


namespace nimu {
namespace algorithms {


graph::graph(size_type v)
  : al(v)
  , v_size{v}
  , e_size{0}
{
}


void graph::add(index_type v, index_type w)
{
  al[v].push_back(w);
  al[w].push_back(v);

  ++e_size;
}

  std::ostream& operator<<(std::ostream& os, graph const& g) {
    os << "V = " << g.vertex_size() << " E = " << g.edge_size() << std::endl;

    for (index_type v = 0; v < g.vertex_size(); ++v) {
      os << "  " << v << ":";

      for (auto o : g.al[v])
        os << " " << o;

      os << std::endl;
    }

    return os;
  }

}
}
