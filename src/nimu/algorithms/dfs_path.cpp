#include <nimu/algorithms/dfs_path.hpp>
#include <nimu/algorithms/graph.hpp>

#include <algorithm>

namespace nimu {
namespace algorithms {


dfs_path::dfs_path(graph const& g, index_type i)
  : visited_(g.vertex_size())
  , edges_(g.vertex_size(), invalid_index)
  , size_{0}
{
  dfs(g, i);
}

bool dfs_path::visited(index_type i) const
{
  return visited_[i];
}

bool dfs_path::connected() const
{
  return size_ == visited_.size();
}

size_type dfs_path::size() const
{
  return size_;
}

auto dfs_path::path(index_type i) const -> path_type
{
  path_type p;
  do {
    p.push_back(i);
    i = edges_[i];
  } while (i != invalid_index);

  std::reverse(p.begin(), p.end());

  return p;
}

void dfs_path::dfs(graph const& g, index_type current)
{
  visited_[current] = true;
  ++size_;

  for (auto i = g.cbegin(current); i != g.cend(current); ++i) {
    auto next = *i;
    if (!visited(next)) {
      edges_[next] = current;
      dfs(g, next);
    }
  }
}


}
}
